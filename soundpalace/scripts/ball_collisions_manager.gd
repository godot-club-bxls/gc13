extends Node

export(NodePath) var _player:NodePath = ''
export(NodePath) var _environment:NodePath = ''
export(int,1,32) var _layer_environment:int = 2
export(int,1,32) var _layer_lighting:int = 3
export(int,1,32) var _layer_fog:int = 4

export(float,0,1000) var _fog_max_distance:int = 10
export(float,0,1000) var _fog_end_min:int = 5
export(float,0,1000) var _fog_end_max:int = 150
export(Color) var _fog_color_min:Color = Color.black

onready var player:RigidBody = get_node(_player)
onready var environment:WorldEnvironment = get_node(_environment)
onready var _fog_max_distance2 = _fog_max_distance*_fog_max_distance
onready var _fog_color_max = environment.environment.fog_color

var flag_environment:int = pow(2,_layer_environment-1)
var flag_lighting:int = pow(2,_layer_lighting-1)
var flag_fog:int = pow(2,_layer_fog-1)

var fog_balls:Array = []

func search_fog_ball( n:Node ) -> void:
	if n == player:
		return
	if n is RigidBody and n.collision_layer & flag_fog != 0:
		fog_balls.append(n)
		return
	for c in n.get_children():
		search_fog_ball(c)
	
func _ready():
	if player != null:
		player.connect( "collide", self, "ball_collision" )
	search_fog_ball( get_parent() )

func rand_hue() -> Color:
	randomize()
	return Color.from_hsv( rand_range(0,1), 1, 1, 1)

func mix_colors( c0:Color, c1:Color, mix:float ) -> Color:
	var xim:float = 1-mix
	return Color.from_hsv( c0.h*xim+c1.h*mix, c0.h*xim+c1.h*mix, c0.v*xim+c1.v*mix, c0.a*xim+c1.a*mix)

func ball_collision( ball:RigidBody ) -> void:
	if environment != null and ball.collision_layer & flag_environment != 0:
		environment.environment.background_sky.sky_top_color = rand_hue()

func _process(delta) -> void:
	
	if player == null:
		return
	
	var fog_closest:float = -1
	for fb in fog_balls:
		var d:float = ( fb.global_transform.origin - player.global_transform.origin ).length_squared()
		if d < _fog_max_distance2 and (fog_closest == -1 or fog_closest > d):
			fog_closest = d
	if fog_closest != -1:
		fog_closest /= _fog_max_distance2
		environment.environment.fog_depth_end = _fog_end_min + (_fog_end_max-_fog_end_min) * fog_closest
		environment.environment.fog_color = mix_colors( _fog_color_min,  _fog_color_max, fog_closest )
		environment.environment.background_color = mix_colors( _fog_color_min,  _fog_color_max, fog_closest )
	else:
		environment.environment.fog_depth_end = _fog_end_max
		environment.environment.fog_color = _fog_color_max
		environment.environment.background_color = _fog_color_max
