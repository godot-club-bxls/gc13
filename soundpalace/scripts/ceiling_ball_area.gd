extends Area

export(NodePath) var _player:NodePath = ''

onready var player:RigidBody = get_node(_player)
var ceiling_balls:Array = []

func _ready() -> void:
	for c in get_parent().get_children():
		if c is RigidBody and c.name.begins_with( "ceiling_ball" ):
			ceiling_balls.append(c)

func _on_ceiling_ball_area_body_entered(body):
	if body != player:
		return
	for cb in ceiling_balls:
		cb.mode = RigidBody.MODE_RIGID
		cb.gravity_scale = 1
	ceiling_balls = []
