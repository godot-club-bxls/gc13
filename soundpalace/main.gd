extends Spatial

func _input(event):
	
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_ESCAPE or event.scancode == KEY_Q:
			get_tree().quit()
