extends RigidBody

export(bool) var play_on_touch:bool = true
export(float) var minimum_speed:float = 0.1
export(float) var falling_stop_delay:float = 0.5

var is_rolling : bool = false
var is_falling : bool = false
var last_position:Vector3
var falling_stop_timer:float = 0

func _ready():
	last_position=translation

func _process(_delta : float):
	
	var dif_ground=(translation-last_position)*Vector3(1,0,1)
	var dif_vert=(translation-last_position)*Vector3(0,1,0)
	
	last_position=translation
	
	if play_on_touch:
		if dif_ground.length() / _delta > minimum_speed:
			if not is_rolling:
				is_rolling=true
				$rolling.play()
		elif is_rolling:
			is_rolling=false
			$rolling.stop()

	if dif_vert.length() / _delta > minimum_speed:
		if not is_falling:
			is_falling=true
			falling_stop_timer = falling_stop_delay
			$falling.play()
	elif is_falling:
		if falling_stop_timer > 0:
			falling_stop_timer -= _delta
		else:
			is_falling=false
			$falling.stop()
