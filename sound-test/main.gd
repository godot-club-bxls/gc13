extends Node2D


func _on_play_button_pressed():
	$fx.play()

func _on_room_size_value_changed(value):
	var reverb = AudioServer.get_bus_effect( 1, 0 )
	reverb.room_size = value


func _on_volume_db_value_changed(value):
	AudioServer.set_bus_volume_db( 1,  value )
	print( value )
